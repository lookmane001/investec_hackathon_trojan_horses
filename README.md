# Investec_Hackathon_Trojan_Horses  :horse:

An open source interface for the decentralized liquidity protocol Aave

Enabling users to:

- Manage and monitor their positions on the Aave Protocol, and the overall status of it
- Manage and monitor their positions on the Aave Safety module
- Participate in the Aave Governance


## Contributing to this repository
We would appreciate your input! We want to make contributing to this repository as easy and transparent as possible, whether it's:

- Reporting any issues related to GUI
- Discussing the current state of the integration between the uniswap and front end user inteface
- Submitting a fix
- Proposing techniques

## Any contributions you make will be under the MIT Software License
In short, when you submit simulation changes, your submissions are understood to be under the same [MIT License](http://choosealicense.com/licenses/mit/) that covers the project. Feel free to contact the maintainers if that's a concern.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
